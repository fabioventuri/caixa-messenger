package adam.caixabank.com.cxbmsg.SplashScreen.model.usecase.imp;

import android.content.SharedPreferences;
import android.provider.Settings;

import com.caixabank.adam.arq.aarqcore.utils.AdamConnectivityManager;

import java.util.concurrent.ExecutionException;

import javax.inject.Inject;

import adam.caixabank.com.commons.utils.crypto.CryptoLibrary;
import adam.caixabank.com.cxbmsg.SplashScreen.BfSplashScreenModule;
import adam.caixabank.com.cxbmsg.SplashScreen.database.DataManager;
import adam.caixabank.com.cxbmsg.SplashScreen.model.usecase.SplashScreenUseCases;
import adam.caixabank.com.cxbmsg.SplashScreen.presenter.SplashScreenPresenter;
import adam.caixabank.com.cxbmsg.SplashScreen.presenter.impl.SplashScreenPresenterImpl;
import adam.caixabank.com.cxbmsg.SplashScreen.repository.retrofit.RetrofitManager;
import adam.caixabank.com.cxbmsg.SplashScreen.repository.retrofit.model.HeaderModel;
import adam.caixabank.com.cxbmsg.dagger.ApplicationContext;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by f.venturi on 08/09/2017.
 */

public class SplashScreenUseCasesImpl implements SplashScreenUseCases {

    @Inject
    SplashScreenPresenterImpl splashScreenPresenter;

    @Override
    public void getServerToken(String androidId, Callback<String> cll) {

        String response = null;

        if (!AdamConnectivityManager.isConnected()) {


            String devideId = CryptoLibrary.parseToMd5(androidId);
            HeaderModel header = new HeaderModel();
            header.setDeviceId(devideId);
            RetrofitManager.API_SERVICE.loadPublicKey().enqueue(cll);

        }
    }

    @Override
    public void savePublicKeySharedPref(String publicKey) {
        SharedPreferences.Editor edit = BfSplashScreenModule.sharedPreferences.edit();
        edit.putString("publicKey", publicKey).commit();
    }

    @Override
    public String getPublicKeyFromSharedPref() {
        return BfSplashScreenModule.sharedPreferences.getString("publicKey", "NOT_FOUND");
    }

}
