package adam.caixabank.com.cxbmsg.initializer;

import com.caixabank.adam.arq.aarqcore.AdamBaseApplication;
import com.caixabank.adam.arq.aarqcore.initializer.AdamInitializerProtocol;
import com.caixabank.adam.arq.aarqcore.utils.AdamConnectivityManager;
import com.caixabank.adam.arq.aarqlog.AdamLog;
import com.caixabank.adam.arq.aarqlog.AdamLogger;
import com.caixabank.adam.arq.aarqlog.LogLevel;
import com.caixabank.adam.arq.aarqrest.client.AdamRestClient;
import com.caixabank.adam.arq.aarqrest.exception.AdamRestException;
import com.caixabank.adam.arq.arqconfig.AdamConfigManager;
import com.caixabank.adam.arq.arqconfig.exception.AdamConfigException;

import javax.inject.Inject;

import adam.caixabank.com.cxbmsg.App;

/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" ----------------------- 
---------------------------------------------------------------------
*/

/**
 * Basic implementation of AdamInitializerProtocol
 * The main purpose of this implementation is to initialize all libraries that need being initialized
 * at the start of application.
 * The method configureInitializers should be called a the onCreate callback of application.
 */
public class ApplicationInitializer implements AdamInitializerProtocol {
    private static final String TAG = "Adam Initializer";

    @Inject
    public AdamLogger adamLogger;
    @Inject
    public AdamConfigManager adamConfigManager;

    public ApplicationInitializer() {

    }

    @Override
    public boolean configureInitializers(AdamBaseApplication adamBaseApplication) {
        ((App) adamBaseApplication).getDaggerComponent().inject(this);
//        initializeAdamLog();
        initializeConnectivityManager(adamBaseApplication);
        AdamLog.debug(TAG, "All foregroung application configured");
        return true;
    }

    @Override
    public boolean configureInitializersInBackground(AdamBaseApplication adamBaseApplication) {
        if ( initializeRest(adamBaseApplication) ) {
            return initializeConfigManager(adamBaseApplication);
            //Aqui la configuración ya está cargada
        }else{
            AdamLog.error(TAG,"Error configuratiog REST client, can't proceed");
            return false;
        }

    }

    private void initializeAdamLog() {
        AdamLog.init(adamLogger);
        AdamLog.setLogLevel(LogLevel.DEBUG);
    }
    private void initializeConnectivityManager(AdamBaseApplication baseApplication) {
        AdamConnectivityManager.init(baseApplication);
    }
    @Override
    public void setCallback(Callback callback) {
        //Not needed
    }

    /**
     * Initialize rest client
     *
     * @param baseApplication
     */
    private boolean initializeRest(AdamBaseApplication baseApplication) {

//            adamRestClient.initialize(baseApplication.getApplicationContext());
            AdamLog.debug(TAG, "Rest initialized in application");
            return true;

    }
    /**
     * Initialize config manager
     */
    private boolean initializeConfigManager(AdamBaseApplication adamBaseApplication) {

        try {
            adamConfigManager.initialize();
            //Once initialized the config;amager is set in the integrationLayer in order to block not allowed functionalities
            adamBaseApplication.addAdamConfigManagerToIntegrationLayer(adamConfigManager);
        } catch (AdamConfigException e) {
            AdamLog.error(TAG,"Error initializing configuration");
        }

        AdamLog.debug(TAG, "Configuration initialized in application");
        return true;

    }

}
