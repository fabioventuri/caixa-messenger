/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package adam.caixabank.com.cxbmsg.SplashScreen;

import android.content.Context;
import android.content.SharedPreferences;

import com.caixabank.adam.arq.aarqcore.AdamBaseApplication;
import com.caixabank.adam.arq.aarqcore.AdamBaseModule;
import com.caixabank.adam.arq.aarqcore.integration.AdamIntegration;

import adam.caixabank.com.commons.utils.Constantes;
import adam.caixabank.com.cxbmsg.SplashScreen.dagger.SplashScreenComponent;
import adam.caixabank.com.cxbmsg.SplashScreen.database.BfSplashScreenSqlLiteHelper;
import adam.caixabank.com.cxbmsg.SplashScreen.database.impl.BfSplashScreenSqlLiteHelperImpl;
import adam.caixabank.com.cxbmsg.dagger.AdamComponent;

public class BfSplashScreenModule extends AdamBaseModule {

    public static final String MODULE_NAME = "BfSplashScreenModule";
    public static BfSplashScreenSqlLiteHelperImpl sqliteHelper;
    private SplashScreenComponent daggerComponent;
    public static SharedPreferences sharedPreferences;

    @Override
    public String getModuleId() {
        return MODULE_NAME;
    }

    @Override
    public void initialize(AdamBaseApplication application) {
        // Startup initialization code
        this.sqliteHelper=new BfSplashScreenSqlLiteHelperImpl(application, "cb-messenger", 1);
        this.sharedPreferences = application.getSharedPreferences(Constantes.CBX_MESSENGER_SHAREDPREF, Context.MODE_PRIVATE);

    }

    @Override
    public void registerPublicActivities(AdamIntegration.IntegrationClient integrationClient) {
        //No public activities
    }

    public SplashScreenComponent getDaggerComponent() {
        return daggerComponent;
    }

    public void setDaggerComponent(SplashScreenComponent daggerComponent) {
        this.daggerComponent = daggerComponent;
    }
}
