/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package adam.caixabank.com.cxbmsg.SplashScreen.dagger;

import android.content.Context;

import com.caixabank.adam.arq.aarqcore.AdamBaseApplication;
import com.caixabank.adam.arq.aarqcore.initializer.AdamInitializerProtocol;
import com.caixabank.adam.arq.aarqlog.AdamLogger;
import com.caixabank.adam.arq.aarqlog.impl.AdamLoggerImpl;
import com.caixabank.adam.arq.aarqrest.client.AdamRestClient;
import com.caixabank.adam.arq.aarqrest.utils.AdamWebViewCookieManagerProxy;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import adam.caixabank.com.commons.utils.ConfigProperties;
import adam.caixabank.com.cxbmsg.R;
import adam.caixabank.com.cxbmsg.SplashScreen.model.usecase.SplashScreenUseCases;
import adam.caixabank.com.cxbmsg.SplashScreen.model.usecase.imp.SplashScreenUseCasesImpl;
import adam.caixabank.com.cxbmsg.SplashScreen.presenter.SplashScreenPresenter;
import adam.caixabank.com.cxbmsg.SplashScreen.presenter.impl.SplashScreenPresenterImpl;
import adam.caixabank.com.cxbmsg.initializer.ApplicationInitializer;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * SplashScreenModule provides the basic dependencies needed in the whole application.
 * New providers that needed to be available at application level can be composed here.
 */
@Module
public class SplashScreenModule {

  @Provides
  @Singleton
  SplashScreenUseCases providesSplashScreenUseCases(){return new SplashScreenUseCasesImpl();}

  @Provides
  @Singleton
  SplashScreenPresenter providesSplashScreenPresenter(SplashScreenUseCases splashScreenUseCases){
    return new SplashScreenPresenterImpl(splashScreenUseCases);}

}
