/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/

package adam.caixabank.com.cxbmsg.dagger;


import android.content.Context;

import com.caixabank.adam.arq.aarqrest.client.AdamRestClient;
import com.caixabank.adam.arq.arqconfig.AdamConfigManager;
import com.caixabank.adam.arq.arqconfig.initializer.data.datasource.ConfigurationCloudDataSource;
import com.caixabank.adam.arq.arqconfig.initializer.data.datasource.ConfigurationDiskDataSource;
import com.caixabank.adam.arq.arqconfig.initializer.data.policy.ConfigurationRepositoryPolicy;
import com.caixabank.adam.arq.arqconfig.initializer.data.policy.impl.ConfigurationRepositoryWithCacheFilePolicy;
import com.caixabank.adam.arq.arqconfig.initializer.data.repository.ConfigurationRepository;
import com.caixabank.adam.arq.arqconfig.initializer.data.repository.impl.ConfigurationRepositoryImpl;


import javax.inject.Singleton;

import adam.caixabank.com.cxbmsg.BuildConfig;
import dagger.Module;
import dagger.Provides;

@Module
public class ConfigModule {


    @Provides
    @Singleton
    ConfigurationDiskDataSource providesConfigurationDiskDataSource(Context context) {
        return new ConfigurationDiskDataSource(context);
    }
    @Provides
    @Singleton
    ConfigurationCloudDataSource providesConfigurationCloudDataSource(AdamRestClient adamRestClient, Context context) {
        return new ConfigurationCloudDataSource(adamRestClient, context);
    }
    @Provides
    @Singleton
    ConfigurationRepositoryPolicy providesConfigurationRepositoryPolicy(ConfigurationCloudDataSource configurationCloudDataSource, ConfigurationDiskDataSource configurationDiskDataSource) {
        return new ConfigurationRepositoryWithCacheFilePolicy(configurationCloudDataSource,configurationDiskDataSource);
    }
    @Provides
    @Singleton
    ConfigurationRepository providesConfigurationRepository(ConfigurationRepositoryPolicy configurationRepositoryPolicy) {
        return new ConfigurationRepositoryImpl(configurationRepositoryPolicy);
    }

    @Provides
    @Singleton
    AdamConfigManager providesAdamConfigManager(ConfigurationRepository configurationRepository) {
        return new AdamConfigManager(configurationRepository, BuildConfig.ENCRYPT_KEY_ID);

    }

}
