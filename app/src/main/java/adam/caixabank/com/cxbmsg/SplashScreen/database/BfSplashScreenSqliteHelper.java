/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package adam.caixabank.com.cxbmsg.SplashScreen.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.caixabank.adam.arq.aarqdata.AdamDataHelper;

import adam.caixabank.com.cxbmsg.SplashScreen.BfSplashScreenModule;

public class BfSplashScreenSqliteHelper extends AdamDataHelper {
    static final int DATABASE_VERSION=1;

    public BfSplashScreenSqliteHelper(Context context) {
        super(context, BfSplashScreenModule.MODULE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        //all the creations instructions
//        db.execSQL(UserPersonalDataContract.PersonalDataScheme.SQL_CREATE_TABLE);
//        db.execSQL(UserContract.UserScheme.SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Delete all tables
//        db.execSQL(UserPersonalDataContract.PersonalDataScheme.SQL_DELETE_TABLE);
//        db.execSQL(UserContract.UserScheme.SQL_DELETE_TABLE);
        //Create again the tables
        onCreate(db);
    }
}
