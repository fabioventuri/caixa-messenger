package adam.caixabank.com.cxbmsg.SplashScreen.repository.retrofit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by b.navarro on 05/09/2017.
 */

public class HeaderModel {


    @SerializedName("X-PublicKey")
    private String pk;

    @SerializedName("X-DeviceId")
    private String deviceId;


    public String getPk() {
        return pk;
    }

    public String getDeviceId() {
        return deviceId;
    }


    public void setPk(String pk) {
        this.pk = pk;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
