package adam.caixabank.com.cxbmsg;

import android.app.Application;
import android.support.v7.app.AppCompatActivity;

import com.caixabank.adam.arq.aarqcore.AdamBaseApplication;
import com.caixabank.adam.arq.aarqcore.initializer.AdamInitializerProtocol;

import javax.inject.Inject;

import adam.caixabank.com.bfenrolment.BfEnrolmentBaseModule;
import adam.caixabank.com.cxbmsg.SplashScreen.BfSplashScreenModule;
import adam.caixabank.com.cxbmsg.SplashScreen.dagger.SplashScreenModule;
import adam.caixabank.com.cxbmsg.dagger.AdamComponent;
import adam.caixabank.com.cxbmsg.dagger.AdamModule;
import adam.caixabank.com.cxbmsg.dagger.DaggerAdamComponent;

/**
 * Created by f.venturi on 08/09/2017.
 */

public class App extends AdamBaseApplication {

    private AdamComponent daggerAppComponent;

    @Inject
    AdamInitializerProtocol applicationInitializer;

    @Override
    public void initializeInjection() {
        daggerAppComponent = DaggerAdamComponent.builder()
                .adamModule(new AdamModule(this))
                .build();
        daggerAppComponent.inject(this);
    }

    @Override
    public void registerApplicationModules() {

        BfSplashScreenModule bfSplashScreenModule = new BfSplashScreenModule();
        registerApplicationModule(bfSplashScreenModule);
        bfSplashScreenModule.setDaggerComponent(daggerAppComponent.bfSplashScreenComponent());

        BfEnrolmentBaseModule bfEnrolmentBaseModule = new BfEnrolmentBaseModule();
        registerApplicationModule(bfEnrolmentBaseModule);
        bfEnrolmentBaseModule.setDaggerComponent(daggerAppComponent.bfEnrolmentComponent());

    }

    @Override
    public void initializeApplication() {
        setApplicationInitializer(applicationInitializer);
    }


    public AdamComponent getDaggerComponent() {
        return daggerAppComponent;
    }
}
