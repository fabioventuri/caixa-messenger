/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package adam.caixabank.com.cxbmsg.SplashScreen.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.caixabank.adam.arq.aarqdata.AdamDataHelper;

public interface BfSplashScreenSqlLiteHelper {
    String initDB();
}
