package adam.caixabank.com.cxbmsg.SplashScreen.repository.retrofit;

import android.os.AsyncTask;



import java.io.IOException;

import adam.caixabank.com.cxbmsg.SplashScreen.repository.retrofit.model.HeaderModel;
import okhttp3.Headers;
import retrofit2.Call;

/**
 * Created by b.navarro on 05/09/2017.
 */

public class Tasks extends AsyncTask<Void, Void, String> {

    public final static String GET_PUBLIC_KEY ="0";
    public final static String POST_SEND_PK_AND_DEVICEID ="1";
    private String callOrder ="";

    Call<Void> call;
    public Tasks(String order, HeaderModel header) {
        callOrder = order;
        switch (order){
            case (GET_PUBLIC_KEY):
//                call  = RetrofitManager.API_SERVICE.loadPublicKey();

                break;
            case POST_SEND_PK_AND_DEVICEID:
                String deviceId = header.getDeviceId();

                call = RetrofitManager.API_SERVICE.sendPublicKeyAndDeviceId(deviceId);
                break;
        }


    }

    @Override
    protected String doInBackground(Void...voids) {

        try {
            retrofit2.Response headerResponsed = call.execute();
            String returnable ="";
            switch (callOrder){
                case GET_PUBLIC_KEY:
                    Headers h = headerResponsed.headers();
                    System.out.println("HEADER "+h.toString());
                    returnable = h.get("X-PublicKey");
                    break;
                case POST_SEND_PK_AND_DEVICEID:
                    returnable = headerResponsed.headers().get("X-Guid");
                    System.out.println("guid ->"+returnable);
                    break;
            }
            return returnable;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
