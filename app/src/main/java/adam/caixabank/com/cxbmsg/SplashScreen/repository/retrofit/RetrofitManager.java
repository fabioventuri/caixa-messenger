package adam.caixabank.com.cxbmsg.SplashScreen.repository.retrofit;

import java.util.concurrent.ExecutionException;

import adam.caixabank.com.cxbmsg.SplashScreen.repository.retrofit.model.HeaderModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by f.venturi on 08/09/2017.
 */

public class RetrofitManager {
    public static APIService API_SERVICE = ServiceGenerator.createService(APIService.class);



    public static String getPublicKey() throws ExecutionException, InterruptedException {
        Tasks tk = new Tasks(Tasks.GET_PUBLIC_KEY, new HeaderModel());
        String res = tk.execute().get();
        return res;
    }

    public static String sendPublicKeyAndDeviceId(String deviceId)throws ExecutionException, InterruptedException {
//        HeaderModel header = new HeaderModel();
//        header.setDeviceId(deviceId);
//        API_SERVICE.loadPublicKey().enqueue(new Callback<String>() {
//            @Override
//            public void onResponse(Call<String> call, Response<Void> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<Void> call, Throwable t) {
//
//            }
//        });
        /*
        Tasks tk = new Tasks(Tasks.POST_SEND_PK_AND_DEVICEID, header);
        String res = tk.execute().get();*/
        return null;
    }
}
