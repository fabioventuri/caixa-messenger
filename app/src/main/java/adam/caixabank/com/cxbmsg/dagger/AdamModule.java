/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package adam.caixabank.com.cxbmsg.dagger;

import android.content.Context;
import android.content.SharedPreferences;

import com.caixabank.adam.arq.aarqcore.AdamBaseApplication;
import com.caixabank.adam.arq.aarqcore.initializer.AdamInitializerProtocol;
import com.caixabank.adam.arq.aarqlog.AdamLogger;
import com.caixabank.adam.arq.aarqlog.impl.AdamLoggerImpl;
import com.caixabank.adam.arq.aarqrest.client.AdamRestClient;
import com.caixabank.adam.arq.aarqrest.utils.AdamWebViewCookieManagerProxy;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import adam.caixabank.com.commons.utils.ConfigProperties;
import adam.caixabank.com.commons.utils.Constantes;
import adam.caixabank.com.cxbmsg.R;
import adam.caixabank.com.cxbmsg.initializer.ApplicationInitializer;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * SplashScreenModule provides the basic dependencies needed in the whole application.
 * New providers that needed to be available at application level can be composed here.
 */
@Module
public class AdamModule {

  private Context context;

  private static final long TIMEOUT_CONNECT_MS = 3000;
  private static final long TIMEOUT_READ_MS = 3000;

  private AdamBaseApplication application;

  /**
   * Constructor.
   *
   * @param application Application context.
   */
  public AdamModule(AdamBaseApplication application) {
    this.application = application;
    this.context = application.getApplicationContext();
  }

  @Provides
  public Context context(){
    return this.context;
  }

  /**
   *
   * @return the Logger implementation
   */
  @Provides
  @Singleton
  AdamLogger providesLogger() {
    return new AdamLoggerImpl();
  }

  /**
   *
   * @return the configuration properties instance.
   * Values are obtained from the config.xml file in the {flavor} folder, so a different
   * configuration can be obtained according to the flavor.
   */
  @Provides
  @Singleton
  ConfigProperties providesConfiguration() {
    ConfigProperties properties = new ConfigProperties();
    properties.setEnvironment(application.getString(R.string.environment));
    properties.setXmlAppsBaseUrl(application.getString(R.string.xml_apps_base_url));
    properties.setOauthBaseUrl(application.getString(R.string.oauth_base_url));
    properties.setShareCookiesWithWebview(application.getResources().getBoolean(R.bool.share_webview_cookies));
    return properties;
  }

  @Provides
  @Singleton
  OkHttpClient providesOkHttpClient(ConfigProperties config) {
    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);

    OkHttpClient.Builder builder = new OkHttpClient.Builder()
            .connectTimeout(TIMEOUT_CONNECT_MS, TimeUnit.MILLISECONDS)
            .readTimeout(TIMEOUT_READ_MS, TimeUnit.MILLISECONDS)
            .addInterceptor(interceptor);

    if (config.isShareCookiesWithWebview()) {
      builder.cookieJar(new AdamWebViewCookieManagerProxy());
    }
    return builder.build();
  }
  @Provides
  @Singleton
  AdamRestClient providesAdamrestClient(OkHttpClient okHttpClient) {
    return new AdamRestClient(okHttpClient);

  }

  @Provides
  AdamInitializerProtocol providesInitializer() {
    return new ApplicationInitializer();
  }

  @Provides
  public SharedPreferences providesSharedPrefrerences(){
    return this.application.getSharedPreferences(Constantes.CBX_MESSENGER_SHAREDPREF, Context.MODE_PRIVATE);
  }

}
