/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package adam.caixabank.com.cxbmsg.SplashScreen.dagger;

import javax.inject.Singleton;

import adam.caixabank.com.cxbmsg.SplashScreen.model.usecase.SplashScreenUseCases;
import adam.caixabank.com.cxbmsg.SplashScreen.view.SplashActivity;
import adam.caixabank.com.cxbmsg.SplashScreen.view.SplashFragment;
import adam.caixabank.com.cxbmsg.dagger.ConfigModule;
import dagger.Subcomponent;


/**
 * Main component for dependencies injection
 */
@Singleton
@Subcomponent(modules = {SplashScreenModule.class})
public interface SplashScreenComponent {
  void inject(SplashFragment splashFragment);
  void inject(SplashScreenUseCases splashScreenUseCases);

}
