/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package adam.caixabank.com.cxbmsg.dagger;

import android.content.SharedPreferences;

import javax.inject.Singleton;

import adam.caixabank.com.bfenrolment.BfEnrolmentComponent;
import adam.caixabank.com.cxbmsg.App;
import adam.caixabank.com.cxbmsg.SplashScreen.dagger.SplashScreenComponent;
import adam.caixabank.com.cxbmsg.SplashScreen.view.SplashActivity;
import adam.caixabank.com.cxbmsg.initializer.ApplicationInitializer;
import dagger.Component;



/**
 * Main component for dependencies injection
 */
@Singleton
@Component(modules = {AdamModule.class, ConfigModule.class})
public interface AdamComponent {
  void inject(App application);
  void inject(SplashActivity splashActivity);
  void inject(ApplicationInitializer applicationInitializer);


  SharedPreferences sharedPreferences();

  //Sub-components must be exposed here
  SplashScreenComponent bfSplashScreenComponent();
  BfEnrolmentComponent bfEnrolmentComponent();

}
