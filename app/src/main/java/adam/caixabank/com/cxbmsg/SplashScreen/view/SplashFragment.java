package adam.caixabank.com.cxbmsg.SplashScreen.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.caixabank.adam.arq.aarqcore.view.fragment.AdamBasePresenterFragment;

import javax.inject.Inject;

import adam.caixabank.com.cxbmsg.R;
import adam.caixabank.com.cxbmsg.SplashScreen.BfSplashScreenModule;
import adam.caixabank.com.cxbmsg.SplashScreen.presenter.SplashScreenPresenter;

/**
 * Created by f.venturi on 04/09/2017.
 */

public class SplashFragment extends AdamBasePresenterFragment<SplashScreenPresenter>
        implements SplashScreenPresenter.View {

    @Inject
    SplashScreenPresenter splashScreenPresenter;

    public static SplashFragment newInstance() {
        SplashFragment fragment = new SplashFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_splashscreen, container, false);
//
        ((BfSplashScreenModule) getRegisteredApplicationModule(BfSplashScreenModule.MODULE_NAME))
                .getDaggerComponent().inject(this);

        setPresenter(splashScreenPresenter);

        getPresenter().setView(this);

        String androidId =Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);

        splashScreenPresenter.getServerToken(androidId);

//        splashScreenPresenter.savePublicKeySharedPref(publicKey);

        Toast.makeText(getActivity(), "Termino", Toast.LENGTH_SHORT).show();

        return layout;
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showError(int errorMessage) {

    }


}
