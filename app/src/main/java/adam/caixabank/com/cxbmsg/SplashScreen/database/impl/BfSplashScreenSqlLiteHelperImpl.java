package adam.caixabank.com.cxbmsg.SplashScreen.database.impl;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import javax.inject.Inject;

import adam.caixabank.com.cxbmsg.dagger.ApplicationContext;
import adam.caixabank.com.cxbmsg.dagger.DatabaseInfo;

/**
 * Created by f.venturi on 08/09/2017.
 */

public class BfSplashScreenSqlLiteHelperImpl extends SQLiteOpenHelper{
    private SQLiteDatabase innerDb;
    public String statusDB = "OFF";
    //PROFILE TABLE




    @Inject
    public BfSplashScreenSqlLiteHelperImpl(@ApplicationContext Context context,
                    @DatabaseInfo String dbName,
                    @DatabaseInfo Integer version) {
        super(context, dbName, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

//        this.innerDb = this.getReadableDatabase();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE_NAME);
        onCreate(db);
    }

    private String profileModelTableCreate(SQLiteDatabase db) {
        try {
            return createDB(db);
//            createProfileModule(db);
//            createVersionModule(db);

        } catch (SQLException e) {
            e.printStackTrace();
            return "Error "+e.toString();

        }
    }


    public String initDB(){

        String status= "OK";
        status = createDB(this.getWritableDatabase());
        return status;
    }




    private String createDB(SQLiteDatabase db){

        db.execSQL("BEGIN TRANSACTION;\n" +
                "\n" +
                "CREATE TABLE IF NOT EXISTS `VersionModel` (\n" +
                "\t`Id`\tinteger NOT NULL,\n" +
                "\t`Version`\tinteger,\n" +
                "\tPRIMARY KEY(`Id`)\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE IF NOT EXISTS `ProfileModel` (\n" +
                "\t`Id`\tinteger NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`Name`\tvarchar,\n" +
                "\t`Surname1`\tvarchar,\n" +
                "\t`Surname2`\tvarchar,\n" +
                "\t`Status`\tvarchar,\n" +
                "\t`Avatar`\tblob,\n" +
                "\t`Number`\tvarchar,\n" +
                "\t`LastIndexUsed`\tvarchar,\n" +
                "\t`UsedIndexes`\tvarchar,\n" +
                "\t`CurrentToken`\tvarchar,\n" +
                "\t`Company`\tvarchar,\n" +
                "\t`CacheUpdated`\tbigint,\n" +
                "\t`ServerId`\tvarchar,\n" +
                "\t`ShowPopUpConversation`\tinteger,\n" +
                "\t`ShowPopUpGroup`\tinteger,\n" +
                "\t`ShowPopUpSearch`\tinteger,\n" +
                "\t`CurrentChannelUri`\tvarchar\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE IF NOT EXISTS `PendingStateMessageModel` (\n" +
                "\t`Id`\tinteger NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`StateType`\tinteger,\n" +
                "\t`IdMessage`\tvarchar,\n" +
                "\t`IdRoom`\tvarchar,\n" +
                "\t`IdState`\tvarchar\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE IF NOT EXISTS `PendingConversationsMessagesModel` (\n" +
                "\t`InternalMessageId`\tinteger,\n" +
                "\t`Id`\tinteger NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`ConversationId`\tinteger,\n" +
                "\t`ContactId`\tinteger,\n" +
                "\t`MessageId`\tvarchar,\n" +
                "\t`Text`\tvarchar,\n" +
                "\t`Downloaded`\tinteger,\n" +
                "\t`DownloadAttempts`\tinteger,\n" +
                "\t`Sent`\tinteger,\n" +
                "\t`ImageFileName`\tvarchar,\n" +
                "\t`ImageThumbnail`\tblob,\n" +
                "\t`Received`\tbigint,\n" +
                "\t`Sended`\tbigint,\n" +
                "\t`Readed`\tbigint,\n" +
                "\t`Added`\tbigint,\n" +
                "\t`IdState`\tvarchar,\n" +
                "    `NotificationSender` varchar,\n" +
                "    `NotificationTarget` varchar,\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE IF NOT EXISTS `ConversationsMessagesModel` (\n" +
                "\t`Id`\tinteger NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`ConversationId`\tinteger,\n" +
                "\t`ContactId`\tinteger,\n" +
                "\t`MessageId`\tvarchar,\n" +
                "\t`Text`\tvarchar,\n" +
                "\t`Downloaded`\tinteger,\n" +
                "\t`DownloadAttempts`\tinteger,\n" +
                "\t`Sent`\tinteger,\n" +
                "\t`ImageFileName`\tvarchar,\n" +
                "\t`ImageThumbnail`\tblob,\n" +
                "\t`Received`\tbigint,\n" +
                "\t`Sended`\tbigint,\n" +
                "\t`Readed`\tbigint,\n" +
                "\t`Added`\tbigint,\n" +
                "\t`IdState`\tvarchar,\n" +
                "    `NotificationSender` varchar,\n" +
                "    `NotificationTarget` varchar,\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE IF NOT EXISTS `ConversationsModel` (\n" +
                "\t`Id`\tinteger NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`Name`\tvarchar,\n" +
                "\t`Status`\tvarchar,\n" +
                "\t`Avatar`\tblob,\n" +
                "\t`Created`\tbigint,\n" +
                "\t`IsGroup`\tinteger,\n" +
                "\t`RoomId`\tvarchar,\n" +
                "\t`Exited`\tbigint,\n" +
                "\t`IndexUsed`\tvarchar,\n" +
                "\t`Updated`\tbigint\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE IF NOT EXISTS `ConversationsContactsModel` (\n" +
                "\t`Id`\tinteger NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`ConversationId`\tinteger,\n" +
                "\t`ContactId`\tinteger,\n" +
                "\t`IsAdmin`\tinteger,\n" +
                "\t`Added`\tbigint,\n" +
                "\t`Exited`\tbigint\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE IF NOT EXISTS `ContactsModel` (\n" +
                "\t`Id`\tinteger NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`Name`\tvarchar,\n" +
                "\t`Surname1`\tvarchar,\n" +
                "\t`Surname2`\tvarchar,\n" +
                "\t`Status`\tvarchar,\n" +
                "\t`Avatar`\tblob,\n" +
                "\t`Number`\tvarchar,\n" +
                "\t`Company`\tvarchar,\n" +
                "\t`Created`\tbigint,\n" +
                "\t`Updated`\tbigint,\n" +
                "\t`Checksum`\tvarchar,\n" +
                "\t`IsEnrolled`\tvarchar,\n" +
                "\t`IsHeader`\tinteger\n" +
                ");\n" +
                "\n" +
                "CREATE INDEX IF NOT EXISTS `PendingConversationsMessagesModel_MessageId` ON `PendingConversationsMessagesModel` (\n" +
                "\t`MessageId`\n" +
                ");\n" +
                "\n" +
                "CREATE INDEX IF NOT EXISTS `PendingConversationsMessagesModel_ConversationId` ON `PendingConversationsMessagesModel` (\n" +
                "\t`ConversationId`\n" +
                ");\n" +
                "\n" +
                "CREATE INDEX IF NOT EXISTS `PendingConversationsMessagesModel_ContactId` ON `PendingConversationsMessagesModel` (\n" +
                "\t`ContactId`\n" +
                ");\n" +
                "\n" +
                "CREATE INDEX IF NOT EXISTS `ConversationsContactsModel_ConversationId` ON `ConversationsContactsModel` (\n" +
                "\t`ConversationId`\n" +
                ");\n" +
                "\n" +
                "CREATE INDEX IF NOT EXISTS `ConversationsContactsModel_ContactId` ON `ConversationsContactsModel` (\n" +
                "\t`ContactId`\n" +
                ");\n" +
                "\n" +
                "COMMIT;");
        return "executed";
    }
}
