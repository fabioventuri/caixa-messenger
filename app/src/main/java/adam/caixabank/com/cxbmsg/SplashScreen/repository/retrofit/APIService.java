package adam.caixabank.com.cxbmsg.SplashScreen.repository.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;


/**
 * Created by b.navarro on 01/09/2017.
 */

public interface APIService {

    @GET("/CMApiWeb/api/v1/security/publicKey/android")
    Call<String> loadPublicKey();

    @POST("/api/v1/security/deviceId")
    Call<Void> sendPublicKeyAndDeviceId(@Header("X-DeviceId") String deviceId);

}
