package adam.caixabank.com.cxbmsg.SplashScreen.database;

import android.content.Context;
import android.content.res.Resources;

import javax.inject.Inject;

import adam.caixabank.com.cxbmsg.SplashScreen.database.impl.BfSplashScreenSqlLiteHelperImpl;
import adam.caixabank.com.cxbmsg.dagger.ApplicationContext;

/**
 * Created by f.venturi on 08/09/2017.
 */

public class DataManager {

    private Context mContext;

    private BfSplashScreenSqlLiteHelperImpl mDbHelper;
    private SharedPreferencesHelper mSharedPrefsHelper;

    @Inject
    public DataManager(@ApplicationContext Context context,
                       BfSplashScreenSqlLiteHelperImpl dbHelper,
                       SharedPreferencesHelper sharedPrefsHelper) {
        mContext = context;
        mDbHelper = dbHelper;
        mSharedPrefsHelper = sharedPrefsHelper;
    }

    public void saveAccessToken(String accessToken) {
        mSharedPrefsHelper.put(SharedPreferencesHelper.PREF_KEY_ACCESS_TOKEN, accessToken);
    }

    public String getAccessToken(){
        return mSharedPrefsHelper.get(SharedPreferencesHelper.PREF_KEY_ACCESS_TOKEN, null);
    }


    public String createLocalDB(){
        String status ="status default";
        status = mDbHelper.initDB();
        return status;
    }

    public void savePublicKey(String key){
        mSharedPrefsHelper.put(SharedPreferencesHelper.PREF_KEY_PUBLIC_KEY, key);
    }

    public String getPublicKey() {
        return mSharedPrefsHelper.get(SharedPreferencesHelper.PREF_KEY_PUBLIC_KEY, null);
    }

    public void saveDeviceId(String deviceId){
        mSharedPrefsHelper.put(SharedPreferencesHelper.PREF_KEY_ANDROID_ID, deviceId);
    }

    public String getDeviceId(){
        return mSharedPrefsHelper.get(SharedPreferencesHelper.PREF_KEY_ANDROID_ID, null);
    }
    public void saveServerToken(String deviceId){
        mSharedPrefsHelper.put(SharedPreferencesHelper.PREF_KEY_SERVER_TOKEN, deviceId);
    }

    public String getServerToken(){
        return mSharedPrefsHelper.get(SharedPreferencesHelper.PREF_KEY_SERVER_TOKEN, null);
    }

}
