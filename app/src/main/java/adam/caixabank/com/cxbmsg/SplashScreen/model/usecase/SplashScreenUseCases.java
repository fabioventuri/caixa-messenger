package adam.caixabank.com.cxbmsg.SplashScreen.model.usecase;

import retrofit2.Callback;

/**
 * Created by f.venturi on 08/09/2017.
 */

public interface SplashScreenUseCases {


    public void savePublicKeySharedPref(String publicKey);

    public String getPublicKeyFromSharedPref();

    public void getServerToken(String deviceId, Callback<String> cll);
}
