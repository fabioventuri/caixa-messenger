/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package adam.caixabank.com.cxbmsg.SplashScreen.presenter.impl;

import android.provider.Settings;
import android.widget.Toast;

import com.caixabank.adam.arq.aarqcore.utils.AdamConnectivityManager;

import java.sql.SQLOutput;

import javax.inject.Inject;
import javax.inject.Singleton;

import adam.caixabank.com.cxbmsg.SplashScreen.model.usecase.SplashScreenUseCases;
import adam.caixabank.com.cxbmsg.SplashScreen.presenter.SplashScreenPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Basic implementation of a presenter.
 * The purpose of this class is to be the intermediate between the view and the model
 */
@Singleton
public class SplashScreenPresenterImpl implements SplashScreenPresenter {

  private View view;
  private SplashScreenUseCases mSplasScreenUserCases;

  @Inject
  public SplashScreenPresenterImpl(SplashScreenUseCases mSplasScreenUserCases) {
    this.mSplasScreenUserCases = mSplasScreenUserCases;
  }

  @Override
  public void setView(View view) {
    this.view = view;

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void destroy() {
    view = null;

  }


  @Override
  public void getServerToken(String androidId) {

    this.mSplasScreenUserCases.getServerToken(androidId, new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                resume();
                System.out.println("SIDGNDSIHNSIDHNJOISDHNISDHNIS "+ response.headers());
            }

        @Override
        public void onFailure(Call<String> call, Throwable t) {
            t.printStackTrace();
        }
        }
    );
  }

  @Override
  public void savePublicKeySharedPref(String publicKey){
    this.mSplasScreenUserCases.savePublicKeySharedPref(publicKey);
  }

  public String getServerTokenResponse(String response){
    System.out.println(response);
    return response;
  }
}
