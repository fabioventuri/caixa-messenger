package adam.caixabank.com.cxbmsg.SplashScreen.view;

import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Slide;
import android.view.Gravity;

import com.caixabank.adam.arq.aarqcore.view.activity.AdamBaseActivity;

import javax.inject.Inject;

import adam.caixabank.com.bfenrolment.BfEnrolmentBaseModule;
import adam.caixabank.com.commons.utils.Constantes;
import adam.caixabank.com.cxbmsg.App;
import adam.caixabank.com.cxbmsg.R;
import adam.caixabank.com.cxbmsg.SplashScreen.BfSplashScreenModule;
import adam.caixabank.com.cxbmsg.SplashScreen.presenter.SplashScreenPresenter;

public class SplashActivity extends AdamBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ((App) getBaseApplication()).getDaggerComponent().inject(this);

        if (savedInstanceState == null) showSplashFragment();

    }

    private void showSplashFragment() {

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment splashFragment = SplashFragment.newInstance();
        ft.replace(R.id.fl_splash_content_frame, splashFragment);
        ft.addToBackStack(null);
        ft.commit();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
