package adam.caixabank.com.bfenrolment;

import com.caixabank.adam.arq.aarqcore.AdamBaseApplication;
import com.caixabank.adam.arq.aarqcore.AdamBaseModule;
import com.caixabank.adam.arq.aarqcore.integration.AdamIntegration;

import dagger.Module;

/**
 * Created by f.venturi on 04/09/2017.
 */

@Module
public class BfEnrolmentBaseModule extends AdamBaseModule {


    public static final String MODULE_NAME = "BfEnrolmentBaseModule";
    private static BfEnrolmentComponent daggerComponent;

    public BfEnrolmentBaseModule() {
    }

    @Override
    public String getModuleId() {
        return MODULE_NAME;
    }

    @Override
    public void initialize(AdamBaseApplication application) {

    }

    @Override
    public void registerPublicActivities(AdamIntegration.IntegrationClient integrationClient) {

    }

    public void setDaggerComponent(BfEnrolmentComponent daggerComponent) {
        this.daggerComponent = daggerComponent;
    }

    public static BfEnrolmentComponent getDaggerComponent() {
        return daggerComponent;
    }
}
