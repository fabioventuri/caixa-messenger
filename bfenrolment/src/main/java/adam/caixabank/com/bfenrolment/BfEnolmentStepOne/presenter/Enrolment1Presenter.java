/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package adam.caixabank.com.bfenrolment.BfEnolmentStepOne.presenter;


import com.caixabank.adam.arq.aarqcore.presenter.AdamPresenter;
import com.caixabank.adam.arq.aarqcore.view.AdamBaseView;

/**
 * Enrolment1 Presenter interface.
 */
public interface Enrolment1Presenter extends AdamPresenter<Enrolment1Presenter.View> {
  /**
   * This is the view Enrolment1.
   */
  interface View extends AdamBaseView {

    void showProgress();

    void hideProgress();

    void showError(int errorMessage);

  }

  void goNextStep();

}
