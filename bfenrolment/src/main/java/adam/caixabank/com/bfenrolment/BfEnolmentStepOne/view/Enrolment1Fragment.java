package adam.caixabank.com.bfenrolment.BfEnolmentStepOne.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import com.caixabank.adam.arq.aarqcore.view.fragment.AdamBasePresenterFragment;

import javax.inject.Inject;

import adam.caixabank.com.bfenrolment.BfEnolmentStepOne.presenter.Enrolment1Presenter;
import adam.caixabank.com.bfenrolment.BfEnrolmentBaseModule;
import adam.caixabank.com.bfenrolment.R;


/**
 * Created by f.venturi on 08/09/2017.
 */

public class Enrolment1Fragment extends AdamBasePresenterFragment<Enrolment1Presenter>
            implements Enrolment1Presenter.View{

        private Button nextButton;

        @Inject
        Context context;

        @Inject
        Enrolment1Presenter enrolment1Presenter;

        public static Enrolment1Fragment newInstance() {
            Enrolment1Fragment fragment = new Enrolment1Fragment();
            Bundle args = new Bundle();
            fragment.setArguments(args);
            return fragment;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                                 @Nullable Bundle savedInstanceState) {

            View layout = inflater.inflate(R.layout.activity_register_step_one, container, false);

            ((BfEnrolmentBaseModule) getRegisteredApplicationModule(BfEnrolmentBaseModule.MODULE_NAME))
                    .getDaggerComponent();

            setPresenter(enrolment1Presenter);

            bindViews(layout);

            return layout;
        }

        public void bindViews(View view){
            nextButton = (Button) view.findViewById(R.id.nextButton);
            nextButton.setOnClickListener(nextButtonOnClickListener);
        }

        View.OnClickListener nextButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        };

        @Override
        public void showProgress() {
        }

        @Override
        public void hideProgress() {

        }

        @Override
        public void showError(int errorMessage) {

        }



        @Override
        public void onResume() {
            super.onResume();
            enrolment1Presenter.setView(this);
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            this.context = context;
        }

}
