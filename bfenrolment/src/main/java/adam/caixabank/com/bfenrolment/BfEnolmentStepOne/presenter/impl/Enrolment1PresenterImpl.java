/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package adam.caixabank.com.bfenrolment.BfEnolmentStepOne.presenter.impl;

import android.view.View;

import javax.inject.Inject;
import javax.inject.Singleton;

import adam.caixabank.com.bfenrolment.BfEnolmentStepOne.presenter.Enrolment1Presenter;

/**
 * Basic implementation of a presenter.
 * The purpose of this class is to be the intermediate between the view and the model
 */
@Singleton
public class Enrolment1PresenterImpl implements Enrolment1Presenter {

  private Enrolment1Presenter.View view;


  @Inject
  public Enrolment1PresenterImpl( ) {

  }

  @Override
  public void setView(View view) {
    this.view = view;

  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void destroy() {
    view = null;

  }

  @Override
  public void goNextStep() {

  }
}
