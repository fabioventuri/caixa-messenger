# Features

Este package contendrá las diferentes subfuncionalidades o "features" del bloque funcional. 
Deberemos tener aquí la clase encargada de:

 * Implementar la clase BaseModule
 * Inicializar aquello que requiera la funcionalidad a desarrollar dentro de commons
 * Listar los activities que deben ser visibles para otras funcionalidades
