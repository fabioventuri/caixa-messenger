package adam.caixabank.com.commons.utils.crypto;

import android.content.Context;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by b.navarro on 06/09/2017.
 */

public class CryptoLibrary {

    private static Context mContext;
    public CryptoLibrary(Context ctx) {
        this.mContext = ctx;
    }

    public static String serverEncrypt(String key, String value) throws InvalidKeySpecException, NoSuchAlgorithmException, UnsupportedEncodingException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        RSAPublicKeySpec pKey = GetPublicKey(key);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PublicKey pk = kf.generatePublic(pKey);
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, pk);
        byte[] encrypted = cipher.doFinal(value.getBytes("UTF-8"));
        return Base64.encodeToString(encrypted, Base64.NO_WRAP);
    }

    public static String deviceDecrypt(String key, String raw) throws NoSuchAlgorithmException {
        String plainText = "";

        try
        {
            if (key.isEmpty()) return plainText;

            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding");
            MessageDigest HAP = MessageDigest.getInstance("MD5");
            HAP.update(key.getBytes());
            byte[] Hash_AES = HAP.digest();

            System.out.println("Hash Length -> " + Hash_AES.length);

            byte[] hash = new byte[32];

            System.arraycopy(Hash_AES, 0, hash, 0, Hash_AES.length);
            System.arraycopy(Hash_AES, 0, hash, 15, Hash_AES.length);

            SecretKeySpec skeySpec = new SecretKeySpec(hash, "AES");

            cipher.init(Cipher.DECRYPT_MODE, skeySpec);

            byte[] buffer = Base64.decode(raw.getBytes("UTF-8"), Base64.NO_WRAP);
            byte[] decrypted = cipher.doFinal(buffer);

            plainText = new String(decrypted, "UTF-8");
            System.out.println("RESULTADO DESCRYPT: " + plainText);

        } catch (Exception exception){
            exception.printStackTrace();
        }

        return plainText;
    }

    public static final String parseToMd5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();


            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private static PublicKey getKey(String key){
        try{
            byte[] byteKey = Base64.decode(key.getBytes(), Base64.DEFAULT);
            X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(byteKey);
            KeyFactory kf = KeyFactory.getInstance("RSA");

            return kf.generatePublic(X509publicKey);
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public static void testGenerateKeys() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        KeyPairGenerator kpg;
        KeyPair kp;
        PublicKey publicKey;
        PrivateKey privateKey;
        byte [] encryptedBytes,decryptedBytes;
        Cipher cipher,cipher1;
        String encrypted,decrypted;
        String  plain="Soy el chico de las poesias";

        try {
            kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(2048);
            kp = kpg.genKeyPair();
            publicKey = kp.getPublic();
            System.out.println("PPublicKey?????"+Base64.encodeToString(publicKey.getEncoded(),Base64.NO_WRAP));
            privateKey = kp.getPrivate();
            System.out.println("PPrivateKey?????"+Base64.encodeToString(privateKey.getEncoded(),Base64.NO_WRAP));

            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            encryptedBytes = cipher.doFinal(plain.getBytes());
            encrypted = Base64.encodeToString(encryptedBytes, Base64.NO_WRAP);
            System.out.println("EEncrypted?????"+encrypted);


            cipher1=Cipher.getInstance("RSA");
            cipher1.init(Cipher.DECRYPT_MODE, privateKey);
            decryptedBytes = cipher1.doFinal(encryptedBytes);
            decrypted = new String(decryptedBytes);
            System.out.println("DDecrypted?????"+decrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    private static RSAPublicKeySpec GetPublicKey(String keyXmlString) throws InvalidKeySpecException, UnsupportedEncodingException, NoSuchAlgorithmException
    {
        //KeyFactory keyFactory = KeyFactory.getInstance("RSA");

        String modulusString = keyXmlString.substring(keyXmlString.indexOf("<Modulus>"), keyXmlString.indexOf("</Modulus>")).replace("<Modulus>", "");
        String exponentString = keyXmlString.substring(keyXmlString.indexOf("<Exponent>"), keyXmlString.indexOf("</Exponent>")).replace("<Exponent>", "");

        byte[] modulusBytes = Base64.decode(modulusString.getBytes("UTF-8"), Base64.DEFAULT);
        byte[] dBytes = Base64.decode(exponentString.getBytes("UTF-8"), Base64.DEFAULT);

        BigInteger modulus = new BigInteger(1, modulusBytes);
        BigInteger d = new BigInteger(1, dBytes);

        RSAPublicKeySpec keySpec = new RSAPublicKeySpec(modulus, d);

        return keySpec;
        //return (RSAPublicKey) keyFactory.generatePublic(keySpec);
    }

}
