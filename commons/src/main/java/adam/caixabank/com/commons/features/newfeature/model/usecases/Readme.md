# Casos de Uso

Este package contendrá las clases que conformarán la lógica de negocio de la feature.

Ejemplo de implementación:

<pre>       
│ usecases
│  ├─ impl
│  │   └── GetWelcomeMessageUseCaseImpl.java
│  ├── GetWelcomeMessageUseCase.java
│  
</pre>
