package adam.caixabank.com.commons.utils;

/**
 * Created by f.venturi on 06/09/2017.
 */

public abstract class Constantes {

    public static final int STEP_ONE = 0;
    public static final int STEP_TWO = 1;
    public static final int STEP_THREE = 2;
    public static final int STEP_FOUR = 3;

    public static final String CBX_MESSENGER_SHAREDPREF = "cbxmessenger";

}
