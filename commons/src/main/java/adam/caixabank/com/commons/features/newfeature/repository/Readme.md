# Repository

Package que contendrá los contratos de los repositorios de datos y sus implementaciones.
 
Ejemplo de implementación:

<pre>        
│ repository
│  ├─ impl
│  │   └── WelcomeRepositoryImpl.java
│  ├── WelcomeRepository.java
│ 
</pre>